//
//  Request.swift
//  Empresas
//
//  Created by Guilherme Araujo on 16/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import Foundation
import Alamofire


/// <#Description#>: This Class is used in all project to help in requests.
class RequestHandle {
    
    static var shared = RequestHandle()
    
    var user: User = User()
    var enterprise: Enterprise = Enterprise()
    var delegate: LoginModelDelegate?
    
    
    /// <#Description#>: Function to config Login Parameters
    /// - Parameters:
    ///   - userMail: InputText By user in View
    ///   - userPassword: InputText By user in View
    func setLoginParametes(userMail:String, userPassword: String){
        user.loginParameters["email"] = userMail
        user.loginParameters["password"] = userPassword
    }
    
    /// <#Description#>: Function to remove parameters by screen before checked.
    /// - Parameters:
    ///   - userMail: InputText By user in View
    ///   - userPassword: InputText By user in View
    func resetParametes(userMail:String, userPassword: String){
        user.loginParameters["email"] = ""
        user.loginParameters["password"] = ""
    }
    
    
    /// <#Description#>: Get in values of login Parameters in Dic format
    func getRequestParameters() -> [String: Any] {
        return user.loginParameters
    }
    
//MARK: REquest Functions
    /// <#Description#>: Function to make a request in API for infos and returning the acess values.
    func realizeLoginRequest(){
        AF.request("https://empresas.ioasys.com.br/api/v1/users/auth/sign_in", method:.post, parameters: user.loginParameters, encoding: JSONEncoding.default).responseJSON { (response) in
                switch response.result {
                    case .success:
                        print("Request Successful")
                        if let headers = response.response?.allHeaderFields as? [String: String],
                            let token = headers["access-token"],
                            let userID = headers["uid"],
                            let client = headers["client"]{
                            self.user.userID = userID
                            self.user.clientResponse = client
                            self.user.accessToken = token
                            self.realizeDelegateSuccess()
                            
                        } else{
                            self.realizeDelegateError()
                            }
                    
                    case let .failure(error):
                        print(error)
            }
            
        }
    }
    
    
    /// <#Description#>: Function to make a request in API using acess values.
    /// - Parameter withName: String with the enterprise name to search.
    func realizeSearchRequest(withName: String) -> String {
        
        let semaphore = DispatchSemaphore (value: 0)
        var requestResult: String?
        var request = URLRequest(url: URL(string: "https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=1&name=\(withName)")!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(user.accessToken!, forHTTPHeaderField: "access-token")
        request.addValue(user.clientResponse!, forHTTPHeaderField: "client")
        request.addValue(user.userID!, forHTTPHeaderField: "uid")

        request.httpMethod = "GET"

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
          guard let data = data else {
            print(String(describing: error))
            return
          }
          print(String(data: data, encoding: .utf8)!)
        requestResult = String(data: data, encoding: .utf8)!
          semaphore.signal()
        }

        task.resume()
        semaphore.wait()
        return requestResult ?? "Not Found"
        }
    
    
//MARK: Delegate Functions
    func realizeDelegateSuccess(){
        if let delegate = delegate {
            delegate.openHomeViewController()
        }
    }
    
    func realizeDelegateError(){
        if let delegate = delegate {
            delegate.invalideParameters()
        }
    }
}
