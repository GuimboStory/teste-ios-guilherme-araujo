//
//  User.swift
//  Empresas
//
//  Created by Guilherme Araujo on 16/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import Foundation


/// <#Description#>: User Data
struct User {
    var userID: String?
    var accessToken: String?
    var clientResponse: String?
    
    var loginParameters: [String: Any] = [
        "email" : "",
        "password" : ""
    ]
    
}
