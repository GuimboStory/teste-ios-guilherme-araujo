//
//  Enterprise.swift
//  Empresas
//
//  Created by Guilherme Araujo on 16/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import Foundation


///Enterprise data
struct Enterprise {
    var id: Int?
    var enterpriseName: String?
    var photo: String?
    var enterpriseDescription:String?
    var city: String?
    var country: String?
    var enterpriseType: String?
    
    var enterpriseParameters: [String: Any] = [
        "name" : "",
        "enterprise_types": "1"
    ]

}
