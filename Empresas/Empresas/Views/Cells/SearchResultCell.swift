//
//  EnterpriseViewCell.swift
//  Empresas
//
//  Created by Guilherme Araujo on 16/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit

class SearchResultCell: UICollectionViewCell{
  
//MARK: View Components
    // Some components use extension contructors.
    var logo: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.image = UIImage(named: "enterprise_bg")
        return image
    }()
    
    var title: UILabel = UILabel(
        text: "",
        textColor: .titleDarkColor,
        numberOfLines: 1,
        lineBreakMode: nil,
        fontType: .boldSystemFont(ofSize: 20),
        alignment: .left
    )
    var subTitle: UILabel = UILabel(
        text: "",
        textColor: .lightGray,
        numberOfLines: 1,
        lineBreakMode: nil,
        fontType: .italicSystemFont(ofSize: 15),
        alignment: .left)
    
    var country: UILabel = UILabel(
        text: "",
        textColor: .lightGray,
        numberOfLines: 1,
        lineBreakMode: nil,
        fontType: .italicSystemFont(ofSize: 15),
        alignment: .left
    )
    
//MARK: Init Configurations
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupView()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: Code View Protocol

/// <#Description#>: This Extension configure the view with CodeView Protocol. For more details see doc in CodeViewProtocol
extension SearchResultCell: CodeViewProtocol{
    func buildViewsInHierarchy() {
        addSubviews([logo,title, subTitle,country])

    }
    
    func setContrains() {
        logo.snp.makeConstraints{ make in
            make.left.top.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().inset(10)
            make.width.equalTo(100)
        }
        
        subTitle.snp.makeConstraints{ make in
            make.left.equalTo(logo.snp.right).offset(15)
            make.centerY.equalToSuperview()
            make.width.equalTo(100)
        }
        
        title.snp.makeConstraints{ make in
            make.left.equalTo(logo.snp.right).offset(15)
            make.bottom.equalTo(subTitle.snp.top).offset(-7)
            make.width.equalTo(100)
        }
        
        
        country.snp.makeConstraints{ make in
            make.left.equalTo(logo.snp.right).offset(15)
            make.top.equalTo(subTitle.snp.bottom).offset(7)
            make.width.equalTo(100)
        }
    }
}


