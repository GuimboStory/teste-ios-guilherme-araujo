//
//  LoginView.swift
//  Empresas
//
//  Created by Guilherme Araujo on 14/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit


/// <#Description#>: This Login Screen is the initial View of the App.
class LoginView: UIView {
    
//MARK: View Components
    // Some components use extension contructors.
    var ioasysLogo: UIImageView = {
        let logo = UIImageView(frame: .zero)
        logo.image = UIImage(named: "logo_home")
        return logo
    }()
    
    var titleLabel: UILabel = UILabel(
        text: "BEM-VINDO AO\n EMPRESAS",
        textColor: .titleDarkColor,
        numberOfLines: 2,
        lineBreakMode: .byWordWrapping,
        fontType: .boldSystemFont(ofSize: 20),
        alignment: .center
    )
    
    var subTitleLabel: UILabel = UILabel(
        text: "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan.",
        textColor: .titleDarkColor,
        numberOfLines: 2,
        lineBreakMode: .byWordWrapping,
        fontType: .boldSystemFont(ofSize: 16),
        alignment: .center
    )
    
    var inputMail: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.minimumFontSize = 17
        textField.contentHorizontalAlignment = .left
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .left
        textField.borderStyle = .roundedRect
        textField.placeholder = "E-mail"
        textField.backgroundColor = .backgroundWhiteRock
        return textField
    }()
    
    
    var inputPassword: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.minimumFontSize = 17
        textField.contentHorizontalAlignment = .left
        textField.contentVerticalAlignment = .center
        textField.textAlignment = .left
        textField.borderStyle = .roundedRect
        textField.placeholder = "Senha"
        textField.backgroundColor = .backgroundWhiteRock
        textField.isSecureTextEntry = true
        return textField
    }()
    
    var loginButton: UIButton = {
        let button = UIButton(type: .custom)
        button.frame = .zero
        button.setTitle("ENTRAR", for: .normal)
        button.titleLabel?.font = .boldSystemFont(ofSize: 20)
        button.setTitleColor(.actionBlueColor, for: .normal)
        button.setTitleColor(.actionBlueClickedColor, for: .highlighted)
        button.titleLabel?.adjustsFontSizeToFitWidth = true
        button.backgroundColor = .backgroundWhiteRock
        button.addTarget(self, action: #selector(clickLogin), for: .touchUpInside)
        return button
    }()
    
    var errorLoginLabel: UILabel = UILabel(text: "",
                            textColor: .actionPink,
                            numberOfLines: 1,
                            lineBreakMode: nil,
                            fontType: .boldSystemFont(ofSize: 13),
                            alignment: .center)

    
    
    
    
//MARK: Init Configurations
    
    var delegate: LoginViewDelegate?
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        self.backgroundColor = .backgroundWhiteRock
        setupView()
    }

    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//MARK: Code View Protocol
/// <#Description#>: This Extension configure the view with CodeView Protocol. For more details see doc in CodeViewProtocol
extension LoginView: CodeViewProtocol{
    
    func buildViewsInHierarchy() {
        addSubviews([
            ioasysLogo, titleLabel,
            subTitleLabel, inputMail,
            inputPassword, loginButton,
            errorLoginLabel
        ])
    }

    func setContrains() {
        
        ioasysLogo.snp.makeConstraints{ make in
            make.height.equalTo(43)
            make.width.equalTo(184.5)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(62.5)
        }
        
        titleLabel.snp.makeConstraints{ make in
            make.height.equalTo(67)
            make.width.equalTo(177)
            make.top.equalTo(ioasysLogo.snp.bottom).offset(48.5)
            make.centerX.equalToSuperview()
        }
        
        subTitleLabel.snp.makeConstraints{ make in
            make.height.equalTo(40)
            make.width.equalTo(283)
            make.top.equalTo(titleLabel.snp.bottom).offset(16.5)
            make.centerX.equalToSuperview()
        }
        
        inputMail.snp.makeConstraints{ make in
            make.height.equalTo(40)
            make.width.equalTo(283)
            make.top.equalTo(subTitleLabel.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
        }
        
        inputPassword.snp.makeConstraints{ make in
            make.height.equalTo(inputMail.snp.height)
            make.width.equalTo(inputMail.snp.width)
            make.top.equalTo(inputMail.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
        }
        
        loginButton.snp.makeConstraints{ make in
            make.height.equalTo(30)
            make.width.equalTo(100)
            make.top.equalTo(inputPassword.snp.bottom).offset(40)
            make.centerX.equalToSuperview()
        }
        
        errorLoginLabel.snp.makeConstraints{ make in
            make.height.equalTo(20)
            make.width.equalTo(283)
            make.top.equalTo(inputPassword.snp.bottom).offset(15)
            make.centerX.equalToSuperview()
        }
        
    }

}

//MARK: Login View Functions
extension LoginView {
    
    @objc
    fileprivate func clickLogin(){
        if let delegate = delegate {
            delegate.realizeLogin()
        }
    }
    
    //Access Functions
    func getMailFromTextField() -> String {
        return inputMail.text!
    }
    
    func getPasswordFromTextField() -> String {
        return inputPassword.text!
    }
    
    func setErrorString(){
        self.errorLoginLabel.text = "Credenciais invalidas, tente novamente."
    }
    
    func cleanErrorString(){
        self.errorLoginLabel.text = ""
    }
    
}
