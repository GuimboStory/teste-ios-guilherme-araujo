//
//  ViewController.swift
//  Empresas
//
//  Created by Guilherme Araujo on 14/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit
import Alamofire

//<#Description#>: The Login Screen - > First ViewController of App.
class ViewController: UIViewController {

    let loginView = LoginView()
    var requestConfig = RequestHandle.shared

    
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = loginView
        loginView.delegate = self
        requestConfig.delegate = self
        // Do any additional setup after loading the view.
    }
    
    
    func openHomeView(){
        let flowLayout = UICollectionViewFlowLayout()
        let controller = HomeViewController(collectionViewLayout: flowLayout)

        navigationController?.pushViewController(controller, animated: true)
    }
    
    func configRequest(){
        let userName = loginView.getMailFromTextField()
        let userPassword = loginView.getPasswordFromTextField()
        requestConfig.setLoginParametes(userMail: userName, userPassword: userPassword)
    }
    
}

extension ViewController: LoginViewDelegate {
    
    func realizeLogin() {
        self.configRequest()
        requestConfig.realizeLoginRequest()
    }
}

extension ViewController: LoginModelDelegate {
    func openHomeViewController() {
        loginView.cleanErrorString()
        self.openHomeView()
    }
    
    func invalideParameters() {
        loginView.setErrorString()
    }
}

