//
//  HomeViewController.swift
//  Empresas
//
//  Created by Guilherme Araujo on 15/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit

/*
 //MARK: TO DO:
 
 - Decode value from search
 - Implement Search Bar
 - Configure Cell of Collection
 - Call DetailView
 - Implement Logout
 */


/// <#Description#>: When has logged. Is possible tu users acess this screen.
class HomeViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let configSearch = RequestHandle.shared
    let cellID = "CellId"
    override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .backgroundWhiteRock
        collectionView?.register(SearchResultCell.self, forCellWithReuseIdentifier: cellID)
        
        let data = configSearch.realizeSearchRequest(withName: "aQm")
        
    }


    //What the Collection cells will looks like
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let resultCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! SearchResultCell
            //Oq terá na cécula
        return resultCell
        }
    
    //Number Of Cells in Collection based in size of two lists
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
        
        }
    
        //Control The size of Cells
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                return CGSize(width: view.frame.width, height: 100)
            }
    
}


