//
//  CodeView.swift
//  Empresas
//
//  Created by Guilherme Araujo on 14/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit
import SnapKit

/// <#Description#>: This protocol helps in creating views by organizing the two essential functions for creating views.
protocol CodeViewProtocol: UIView{
    /// <#Description#>: Buid Views Hierarchy with all subViews in View.
    func buildViewsInHierarchy()
    
    /// <#Description#>: Set Contrains of Components in View.
    func setContrains()
    
    /// <#Description#>: Function to be call inside of init function of View.
    func setupView()
}


//
extension CodeViewProtocol{
    
    //call buildViewsInHierarchy and setContrains functions.
    func setupView(){
        buildViewsInHierarchy()
        setContrains()
    }
    
}
