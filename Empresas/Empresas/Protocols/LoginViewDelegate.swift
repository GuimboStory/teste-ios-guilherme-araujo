//
//  LoginDelegate.swift
//  Empresas
//
//  Created by Guilherme Araujo on 15/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit

/// <#Description#>: Method to call in View when the the button has pressed.
protocol LoginViewDelegate {
    func realizeLogin()
}
