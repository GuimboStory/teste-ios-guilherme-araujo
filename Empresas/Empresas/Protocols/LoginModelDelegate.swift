//
//  LoginModelDelegate.swift
//  Empresas
//
//  Created by Guilherme Araujo on 16/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import Foundation

/// <#Description#>: Protocol to access Methods in Handler
protocol LoginModelDelegate {
    
    /// <#Description#>: This Method is Implementred by Controller and user to open window after login.
    func openHomeViewController()
    
    /// <#Description#>: Case the credentials are invalidate, is passed second option to Model.
    func invalideParameters()
}
