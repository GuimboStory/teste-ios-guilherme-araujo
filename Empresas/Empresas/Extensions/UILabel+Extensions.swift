//
//  UILabel.swift
//  Empresas
//
//  Created by Guilherme Araujo on 14/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit

/// <#Description#>: Extension for UILabel where it allows to facilitate the creation of new labels passing some essential parameters to the project
extension UILabel {
    
    convenience init(text: String, textColor: UIColor, numberOfLines: Int?, lineBreakMode: NSLineBreakMode?, fontType: UIFont, alignment: NSTextAlignment) {
        self.init()
        self.translatesAutoresizingMaskIntoConstraints = false
        self.text = text
        self.textColor = textColor
        self.font = fontType
        self.textAlignment = alignment
        
        
        if let lines = numberOfLines {
            self.numberOfLines = lines
        }
        
        if let lineBreak = lineBreakMode {
            self.lineBreakMode = lineBreak
        }
    }
    
}
