//
//  UIView+Extensions.swift
//  Empresas
//
//  Created by Guilherme Araujo on 14/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import UIKit

/// <#Description#>: This extension contains a function to assist in creating Views.
extension UIView {
    
    /// <#Description#>:  Allows  add multiple subViews to a View
    /// - Parameter views: Array of UIViews
    func addSubviews(_ views: [UIView]){
        views.forEach{ (view) in
            self.addSubview(view)
        }
    }
}
