//
//  UIColor+Extensions.swift
//  Empresas
//
//  Created by Guilherme Araujo on 14/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import Foundation
import UIKit

/// <#Description#>: This extension groups together some of the standard colors of the project
/// (with the exception of black and white) to help with code implementation
extension UIColor {
    static let backgroundWhiteRock = #colorLiteral(red: 0.9215686275, green: 0.9137254902, blue: 0.8431372549, alpha: 1)
    static let backgroundDarkRock = #colorLiteral(red: 0.7098039216, green: 0.7058823529, blue: 0.6588235294, alpha: 1)
    static let titleDarkColor = #colorLiteral(red: 0.2196078431, green: 0.2156862745, blue: 0.262745098, alpha: 1)
    static let subTitleGray = #colorLiteral(red: 0.6745098039, green: 0.6705882353, blue: 0.6705882353, alpha: 1)
    static let actionBlueColor = #colorLiteral(red: 0.3411764706, green: 0.7333333333, blue: 0.737254902, alpha: 1)
    static let actionBlueClickedColor = #colorLiteral(red: 0.2073570437, green: 0.4458818999, blue: 0.4500959553, alpha: 1)
    static let actionPink = #colorLiteral(red: 0.9333333333, green: 0.2980392157, blue: 0.4666666667, alpha: 1)
    static let barDarkPink = #colorLiteral(red: 0.653581621, green: 0.2716318955, blue: 0.4666666667, alpha: 1)
}

