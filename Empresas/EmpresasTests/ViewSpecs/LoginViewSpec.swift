//
//  LoginViewSpec.swift
//  EmpresasTests
//
//  Created by Guilherme Araujo on 14/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots
@testable import Empresas


//Testing The construction of HomeView
class HomeViewSpec: QuickSpec {
    override func spec() {
        describe("This test is for Home View"){
            it("The view is inconsistent with the previously selected pattern"){
                let frame = CGRect(x: 0, y: 0, width: 360, height: 660)
                let view  = LoginView(frame: frame)
                expect(view) == snapshot("LoginView")
            }
            
        }
    }
}
