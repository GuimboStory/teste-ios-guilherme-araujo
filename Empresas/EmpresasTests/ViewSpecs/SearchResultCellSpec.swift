//
//  SearchResultCellSpec.swift
//  EmpresasTests
//
//  Created by Guilherme Araujo on 16/02/20.
//  Copyright © 2020 Guilherme Araujo. All rights reserved.
//

import Quick
import Nimble
import Nimble_Snapshots
@testable import Empresas


//Testing The construction of HomeView
class SearchResultCellSpec: QuickSpec {
    override func spec() {
        describe("This test is for Home View"){
            it("The view is inconsistent with the previously selected pattern"){
                let frame = CGRect(x: 0, y: 0, width: 330, height: 100)
                let view  = SearchResultCell(frame: frame)
                view.title.text = "Empresa1"
                view.subTitle.text = "Consultoria"
                view.country.text = "Brazil"
                expect(view) == snapshot("SearchResultCell")
            }
            
        }
    }
}
